import _ from 'lodash';
import Vue from 'vue';
import Router from 'vue-router';
import {HOME, TODO_LIST_CREATE, TODO_LIST_EDIT, TODO_LIST_VIEW} from "@/enum/ROUTES";


const TodoLists = () => import('@/views/TodoLists');
const TodoListView = () => import('@/views/TodoListView');
const TodoListEdit = () => import('@/views/TodoListEdit');

Vue.use(Router);

export default new Router({
  mode: 'hash',
  routes: [
    _.assign({}, HOME, {
      component: TodoLists
    }),
    _.assign({}, TODO_LIST_EDIT, {
      component: TodoListEdit
    }),
    _.assign({}, TODO_LIST_CREATE, {
      component: TodoListEdit
    }),
    _.assign({}, TODO_LIST_VIEW, {
      component: TodoListView
    })
  ]
});

import _ from 'lodash';
import MESSAGE_TYPES from '../../enum/MESSAGE_TYPES';

export default (spec) => {
  return _.assign({
    type: MESSAGE_TYPES.INFO,
    key: undefined,
    localeKey: undefined,
    message: undefined
  }, spec);
};

import _ from 'lodash';

export default (spec) => {
  return _.assign({
    tid: null,
    todoType: 'todo-item',
    todoName: null,
    todoCompleted: false
  }, spec);
};

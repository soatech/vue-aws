// ========================================================
// Imports
// ========================================================

import _ from 'lodash';

// ========================================================
// Private Members
// ========================================================

const isDevMode = () => {
  const env = process.env.NODE_ENV || 'development';

  return _.includes(env, "dev");
};

// ========================================================
// Actions
// ========================================================

const actions = {
};

// ========================================================
// Mutations
// ========================================================

const mutations = {
};

// ========================================================
// Getters
// ========================================================

const getters = {
  isDevMode,
  showMessageDetails(state) {
    return state.showMessageDetails;
  },
  enableConsoleLog(state) {
    return state.enableConsoleLog;
  }
};

// ========================================================
// State
// ========================================================

const state = {
  showMessageDetails: isDevMode(),
  enableConsoleLog: isDevMode()
};

// ========================================================
// Public Interface
// ========================================================

export default {
  actions,
  mutations,
  getters,
  state
};

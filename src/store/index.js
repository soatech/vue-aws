// ========================================================
// Imports
// ========================================================

import Vue from 'vue';
import Vuex from 'vuex';

import TodoStore from './TodoStore';
import TodoListStore from './TodoListStore';
import MessageStore from './MessageStore';
import LoadingStore from './LoadingStore';
import ConfigStore from "@/store/ConfigStore";

Vue.use(Vuex);

// ========================================================
// Store
// ========================================================

const store = new Vuex.Store({
  modules: {
    config: ConfigStore,
    loading: LoadingStore,
    messages: MessageStore,
    todoLists: TodoListStore,
    todos: TodoStore
  }
});

// ========================================================
// Public Interface
// ========================================================

export default store;

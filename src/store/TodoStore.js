// ========================================================
// Imports
// ========================================================

import _ from 'lodash';
import moment from 'moment';
import Vue from 'vue';
import COMMITS from '@/enum/COMMITS';
import {deleteTodo, getTodos, postTodo} from '@/services/TodoService';
import LOADING_KEYS from "@/enum/LOADING_KEYS";
import MessageSchema from "@/store/models/MessageSchema";
import MESSAGE_KEYS from "@/enum/MESSAGE_KEYS";
import MESSAGE_TYPES from "@/enum/MESSAGE_TYPES";

// ========================================================
// Actions
// ========================================================

const actions = {
  removeTodo({commit}, {tid}) {
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODOS.DELETE_TODO);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODOS.DELETE_TODO_FAILED
    });

    return deleteTodo(tid)
      .then((todoItem) => {
        commit(COMMITS.TODOS.DELETE, tid);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODOS.DELETE_TODO);

        return todoItem;
      })
      .catch((error) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODOS.DELETE_TODO);

        // TODO: i18n
        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODOS.DELETE_TODO_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to delete Todo',
          detail: error.message
        }));
      });
  },

  loadTodos({commit}, {tlid}) {
    console.log('loadTodos');
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODOS.GET_TODOS);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODOS.GET_TODOS_FAILED
    });

    return getTodos(tlid)
      .then((todoItems) => {
        commit(COMMITS.TODOS.LOAD, todoItems);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODOS.GET_TODOS);

        return todoItems;
      })
      .catch((error) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODOS.GET_TODOS);

        // TODO: i18n
        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODOS.GET_TODOS_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to retrieve Todos',
          detail: error.message
        }));
      });
  },

  saveTodo({commit}, {todoItem}) {
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODOS.SAVE_TODO);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODOS.SAVE_TODO_FAILED
    });

    return postTodo(todoItem)
      .then((updatedTodoItem) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODOS.SAVE_TODO);
        commit(COMMITS.TODOS.SET_TODO, updatedTodoItem);

        return updatedTodoItem;
      })
      .catch((error) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODOS.SAVE_TODO);

        // TODO: i18n
        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODO_LISTS.SAVE_TODO_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to save Todo',
          detail: error.message
        }));
      });
  },
};

// ========================================================
// Mutations
// ========================================================

const mutations = {
  [COMMITS.TODOS.DELETE](state, tid) {
    Vue.delete(state.items, tid);
    state.lastUpdated = moment();
  },

  [COMMITS.TODOS.LOAD](state, todoItems) {
    state.items = {};

    _.each(todoItems, (todoItem) => {
      state.items[todoItem.tid] = todoItem;
    });

    state.lastUpdated = moment();
  },

  [COMMITS.TODOS.CLEAR](state) {
    state.items = {};
    state.lastUpdated = moment();
  },

  [COMMITS.TODOS.SET_TODO](state, updatedTodoItem) {
    Vue.set(state.items, updatedTodoItem.tid, updatedTodoItem);
    state.lastUpdated = moment();
  }
};

// ========================================================
// Getters
// ========================================================

const getters = {
  isTodosExpired(state) {
    // do a moment() diff to see if it's a set amount old
    return false;
  },
  todoItems(state) {
    return state.items;
  },
  todoItemsByList(state) {
    return (tlid) => {
      return _.filter(state.items, (item) => {
        return item.tlid === tlid;
      }) || [];
    };
  }
};

// ========================================================
// State
// ========================================================

const state = {
  items: {},
  lastUpdated: undefined
};

// ========================================================
// Public Interface
// ========================================================

export default {
  actions,
  mutations,
  getters,
  state
};

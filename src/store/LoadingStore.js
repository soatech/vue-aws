// ========================================================
// Imports
// ========================================================

import _ from 'lodash';
import Vue from 'vue';
import COMMITS from '../enum/COMMITS';

// ========================================================
// Private Members
// ========================================================

// ========================================================
// Actions
// ========================================================

const actions = {
  addLoading({commit}, key) {
    commit(COMMITS.LOADING.ADD_LOADING, key);
  },
  clearLoading({commit}) {
    commit(COMMITS.LOADING.CLEAR_LOADING);
  },
  removeLoading({commit}, key) {
    commit(COMMITS.LOADING.REMOVE_LOADING, key);
  }
};

// ========================================================
// Mutations
// ========================================================

const mutations = {
  [COMMITS.LOADING.ADD_LOADING](state, key) {
    Vue.set(state, key, true);
  },
  [COMMITS.LOADING.CLEAR_LOADING](state) {
    // We need to mutate the state object.  If we just set
    // it back to {} then it wont detect the change
    _.each(_.keys(state), (k) => {
      Vue.delete(state, k);
    });
  },
  [COMMITS.LOADING.REMOVE_LOADING](state, key) {
    Vue.delete(state, key);
  }
};

// ========================================================
// Getters
// ========================================================

const getters = {
  isLoading(state) {
    return _.size(state) >= 1;
  },
  isLoadingByKey(state) {
    return (key) => {
      if (key) {
        return _.get(state, key, false);
      }

      return _.size(state) >= 1;
    };
  }
};

// ========================================================
// State
// ========================================================

const state = {};

// ========================================================
// Public Interface
// ========================================================

export default {
  actions,
  mutations,
  getters,
  state
};

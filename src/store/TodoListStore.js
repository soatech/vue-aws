// ========================================================
// Imports
// ========================================================

import _ from 'lodash';
import moment from 'moment';
import Vue from 'vue';
import COMMITS from '@/enum/COMMITS';
import {deleteTodoList, getTodoList, getTodoLists, postTodoList} from '@/services/TodoListService';
import LOADING_KEYS from "@/enum/LOADING_KEYS";
import MessageSchema from "@/store/models/MessageSchema";
import MESSAGE_KEYS from "@/enum/MESSAGE_KEYS";
import MESSAGE_TYPES from "@/enum/MESSAGE_TYPES";

// ========================================================
// Actions
// ========================================================

const actions = {
  removeTodoList({commit}, {tlid}) {
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODO_LISTS.DELETE_TODO_LIST);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODO_LISTS.DELETE_TODO_LIST_FAILED
    });

    return deleteTodoList(tlid)
      .then((todoListItem) => {
        commit(COMMITS.TODO_LISTS.DELETE, tlid);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.DELETE_TODO_LIST);

        return todoListItem;
      })
      .catch((error) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.DELETE_TODO_LIST);

        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODO_LISTS.DELETE_TODO_LIST_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to delete Todo List',
          detail: error.message
        }));
      });
  },

  loadTodoList({commit}, {tlid}) {
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODO_LISTS.GET_TODO_LIST);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODO_LISTS.GET_TODO_LIST_FAILED
    });

    return getTodoList(tlid)
      .then((todoListItem) => {
        commit(COMMITS.TODO_LISTS.SET_TODO_LIST, todoListItem);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.GET_TODO_LIST);

        return todoListItem;
      })
      .catch((error) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.GET_TODO_LIST);

        // TODO: i18n
        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODO_LISTS.GET_TODO_LIST_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to retrieve Todo List',
          detail: error.message
        }));
      });
  },

  loadTodoLists({commit}) {
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODO_LISTS.GET_TODO_LISTS);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODO_LISTS.GET_TODO_LISTS_FAILED
    });

    return getTodoLists()
      .then((todoLists) => {
        commit(COMMITS.TODO_LISTS.LOAD_ALL, todoLists);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.GET_TODO_LISTS);

        return todoLists;
      })
      .catch((error) => {
        commit(COMMITS.TODO_LISTS.CLEAR);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.GET_TODO_LISTS);

        // TODO: i18n
        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODO_LISTS.GET_TODO_LISTS_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to retrieve Todo Lists',
          detail: error.message
        }));
      });
  },

  saveTodoList({commit}, {todoListItem}) {
    commit(COMMITS.LOADING.ADD_LOADING, LOADING_KEYS.TODO_LISTS.SAVE_TODO_LIST);
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, {
      key: MESSAGE_KEYS.TODO_LISTS.SAVE_TODO_LIST_FAILED
    });

    return postTodoList(todoListItem)
      .then((updatedTodoListItem) => {
        commit(COMMITS.TODO_LISTS.SET_TODO_LIST, updatedTodoListItem);
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.SAVE_TODO_LIST);

        return updatedTodoListItem;
      })
      .catch((error) => {
        commit(COMMITS.LOADING.REMOVE_LOADING, LOADING_KEYS.TODO_LISTS.SAVE_TODO_LIST);

        // TODO: i18n
        commit(COMMITS.MESSAGES.ADD_MESSAGE, MessageSchema({
          key: MESSAGE_KEYS.TODO_LISTS.SAVE_TODO_LIST_FAILED,
          type: MESSAGE_TYPES.ERROR,
          message: 'Failed to save Todo List',
          detail: error.message
        }));
      });
  }
};

// ========================================================
// Mutations
// ========================================================

const mutations = {
  [COMMITS.TODO_LISTS.DELETE](state, tlid) {
    Vue.delete(state.items, tlid);
    state.lastUpdated = moment();
  },
  [COMMITS.TODO_LISTS.LOAD_ALL](state, todoLists) {
    state.items = {};

    _.each(todoLists, (todoListItem) => {
      state.items[todoListItem.tlid] = todoListItem;
    });
    state.lastUpdated = moment();
  },
  [COMMITS.TODO_LISTS.CLEAR](state) {
    state.items = {};
    state.lastUpdated = moment();
  },
  [COMMITS.TODO_LISTS.SET_TODO_LIST](state, updatedTodoListItem) {
    Vue.set(state.items, updatedTodoListItem.tlid, updatedTodoListItem);
    state.lastUpdated = moment();
  }
};

// ========================================================
// Getters
// ========================================================

const getters = {
  isTodoListsExpired(state) {
    // do a moment() diff to see if it's a set amount old
    return false;
  },
  todoListItems(state) {
    return state.items;
  },
  todoListItemById(state) {
    return (tlid) => {
      return _.find(state.items, (item) => {
        return (_.eq(item.tlid, tlid) || _.eq(item.listUrl, tlid));
      });
    };
  }
};

// ========================================================
// State
// ========================================================

const state = {
  items: {},
  lastUpdated: undefined
};

// ========================================================
// Public Interface
// ========================================================

export default {
  actions,
  mutations,
  getters,
  state
};

// ========================================================
// Imports
// ========================================================

import _ from 'lodash';
import Vue from 'vue';
import COMMITS from '../enum/COMMITS';
import MessageSchema from './models/MessageSchema';

// ========================================================
// Private Members
// ========================================================

// ========================================================
// Actions
// ========================================================

const actions = {
  addMessage({commit}, payload) {
    commit(COMMITS.MESSAGES.ADD_MESSAGE, payload);
  },
  clearMessages({commit}) {
    commit(COMMITS.MESSAGES.CLEAR_MESSAGE);
  },
  removeMessage({commit}, payload) {
    commit(COMMITS.MESSAGES.REMOVE_MESSAGE, payload);
  }
};

// ========================================================
// Mutations
// ========================================================

const mutations = {
  [COMMITS.MESSAGES.ADD_MESSAGE](state, payload) {
    const messageModel = MessageSchema(payload);
    Vue.set(state, messageModel.key, messageModel);
  },
  [COMMITS.MESSAGES.CLEAR_MESSAGE](state) {
    // We need to mutate the state object.  If we just set
    // it back to {} then it wont detect the change
    _.each(_.keys(state), (k) => {
      Vue.delete(state, k);
    });
  },
  [COMMITS.MESSAGES.REMOVE_MESSAGE](state, {key}) {
    Vue.delete(state, key);
  }
};

// ========================================================
// Getters
// ========================================================

const getters = {
  topMessage(state) {
    if(_.size(state) >= 1) {
      return _.values(state)[0];
    }

    return null;
  },
  getMessageByKey(state) {
    return (key) => {
      if (key) {
        return _.get(state, key);
      }

      return null;
    };
  }
};

// ========================================================
// State
// ========================================================

const state = {};

// ========================================================
// Public Interface
// ========================================================

export default {
  actions,
  mutations,
  getters,
  state
};

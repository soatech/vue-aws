import _ from 'lodash';

/**
 *
 * @param {object} vue - Instance of Vue component
 * @param {String} field - Name of field being tested
 * @returns {function()} Rule function returning true or error string
 */
const isRequired = ({vue, field}) => {
  return (val) => {
    if(vue._isBeingDestroyed || vue._isDestroyed) {
      return true;
    }

    if (_.isEmpty(val)) {
      return vue.$t('rules.isRequired', {field});
    }

    return true;
  };
};

/**
 *
 * @param {object} vue - Instance of Vue component
 * @param {String} field - Name of field being tested
 * @returns {function()} Rule function returning true or error string
 */
const validEmail = ({vue, field}) => {
  return (val) => {
    if(vue._isBeingDestroyed || vue._isDestroyed) {
      return true;
    }

    if(val && val.search(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/) === -1) {
      return vue.$t('rules.validEmail', {field});
    }

    return true;
  };
};

/**
 *
 * @param {object} vue - Instance of Vue component
 * @returns {function(*=)}
 */
const validPassword = ({vue}) => {
  return (val) => {
    // Min length
    // numbers
    // special characters
    // mixed case

    if(vue._isBeingDestroyed || vue._isDestroyed) {
      return true;
    }

    const hasMinLength = (val) => _.size(val) >= 8;

    const hasMixedCase = (val) => ((val.search(/[A-Z]/) !== -1) && (val.search(/[a-z]/) !== -1));

    const hasNumbers = (val) => val.search(/[0-9]/) !== -1;

    const hasSpecialCharacters = (val) => val.search(/[ !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]/) !== -1;

    if(val
      && (!hasMinLength(val)
        || !hasMixedCase(val)
        || !hasNumbers(val)
        || !hasSpecialCharacters(val))) {
      return vue.$t('rules.validPassword');
    }

    return true;
  };
};

const ALL_RULES = {
  isRequired,
  validEmail,
  validPassword
};

export function mapRules(rules) {
  return _.pick(ALL_RULES, rules);
}
export default ALL_RULES;

const _HOME = {
  path: '/',
  name: 'HOME'
};
const _TODO_LIST_VIEW = {
  path: '/:tlid',
  name: 'TODO_LIST_VIEW'
};
const _TODO_LIST_EDIT = {
  path: '/:tlid/edit',
  name: 'TODO_LIST_EDIT'
};
const _TODO_LIST_CREATE = {
  path: '/create',
  name: 'TODO_LIST_CREATE'
};

const _ALL_ROUTES = {
  HOME: _HOME,
  TODO_LIST_VIEW: _TODO_LIST_VIEW,
  TODO_LIST_EDIT: _TODO_LIST_EDIT,
  TODO_LIST_CREATE: _TODO_LIST_CREATE
};

export const HOME = _HOME;
export const TODO_LIST_VIEW = _TODO_LIST_VIEW;
export const TODO_LIST_EDIT = _TODO_LIST_EDIT;
export const TODO_LIST_CREATE = _TODO_LIST_CREATE;

export default _ALL_ROUTES;


// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue';
import { sync } from 'vuex-router-sync';

import Vuetify from 'vuetify';

import App from './App';
import router from './router';
import store from './store';
import i18n from './locales';

sync(store, router);

Vue.use(Vuetify);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
});

export const errorHandler = (error) => {
  console.error(error);
  throw new Error(error.message);
};

import config from './config';
import axios from 'axios';
import {errorHandler} from '../shared';

const url = config.API_URL;
const client = axios.create({
  baseUrl: url,
  headers: {
    'x-api-key': config.API_KEY
  }
});

export const getTodoList = (tlid) => {
  return client.get(`${url}/${tlid}`)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

export const getTodoLists = () => {
  return client.get(url)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

export const postTodoList = (data) => {
  return client.post(url, data)
    .then((response) => {
      // should be the updated/added TodoListItem
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

export const deleteTodoList = (tlid) => {
  return client.delete(`${url}/${tlid}`)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

import devConfig from './dev.config';
import testConfig from './test.config';
import prodConfig from './prod.config';
import _ from 'lodash';

const config = devConfig;
const env = process.env.NODE_ENV || 'development';
config.API_KEY = _.get(process, ['env', 'AWS_API_KEY']);

if (_.includes(env, 'dev')) {
  _.assign(config, devConfig);
} else if (_.includes(env, 'test')) {
  _.assign(config, testConfig);
} else if (_.includes(env, 'prod')) {
  _.assign(config, prodConfig);
}

export default config;

import config from './config';
import axios from 'axios';
import {errorHandler} from '../shared';

const url = config.API_URL;
const client = axios.create({
  baseUrl: url,
  headers: {
    'x-api-key': config.API_KEY
  }
});

export const getTodos = (tlid) => {
  return client.get(`${url}/${tlid}`)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

export const postTodo = (data) => {
  return client.post(url, data)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

export const deleteTodo = (tid) => {
  return client.delete(`${url}/${tid}`)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      errorHandler(error);
    });
};

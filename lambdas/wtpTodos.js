'use strict';

console.log('Loading function');

// Normal DynamoDB
const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();

const readDb = dynamo;
const writeDb = dynamo;

/**
 * Demonstrates a simple HTTP endpoint using API Gateway. You have full
 * access to the request and response payload, including headers and
 * status code.
 *
 * To scan a DynamoDB table, make a GET request with the TableName as a
 * query string parameter. To put, update, or delete an item, make a POST,
 * PUT, or DELETE request respectively, passing in the payload to the
 * DynamoDB API as a JSON body.
 */
exports.handler = (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  const done = (err, res) => callback(null, {
    statusCode: err ? '400' : '200',
    body: err ? err.message : JSON.stringify(res),
    headers: {
      'Content-Type': 'application/json',
      'Allow': 'OPTIONS, GET, HEAD, POST, PUT, DELETE',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'OPTIONS, GET, HEAD, POST, PUT, DELETE'
    }
  });

  console.log('Context:', JSON.stringify(context, null, 2));
  // TODO: get lodash working for _.get() here
  const baseConfig = {
    TableName: event.stageVariables.TABLE_NAME || 'wtp-todos-dev'
  };
  let config = null;
  let tid = null;

  switch (event.httpMethod) {
    case 'DELETE':
      // check for key on path:
      tid = event.pathParameters && event.pathParameters.proxy;
      let TodoItem = {};

      if(tid && tid.length) {
        TodoItem.tid = tid;
      } else {
        TodoItem = JSON.parse(event.body);
      }

      config = Object.assign({}, baseConfig, {
        Key: TodoItem
      });

      writeDb.deleteItem(config, done);
      break;
    case 'GET':
      // if ID is provided, then lookup a single entry
      const index = event.pathParameters && event.pathParameters.proxy;

      if(index && index.length) {
        config = Object.assign({}, baseConfig, {
          "FilterExpression": "tid = :index OR tlid = :index",
          "ExpressionAttributeValues": {
            ":index": index
          }
        });

        console.log(config);

        readDb.scan(config, (err, res) => {
          if(err) {
            done(err, res);
          }

          if(!res) {
            done(new Error("Invalid Request (no res)"));
          }

          let retVal = res.Items;
          let todoItem = null;

          if(res.Count === 1) {
            todoItem = res.Items[0];

            if(index === todoItem.tid) {
              retVal = res.Items[0];
            }
          }

          done(err, retVal);
        });
      } else {
        done(new Error("Invalid Request (no index)"));
      }
      break;
    case 'POST':
      const item = JSON.parse(event.body);

      if(!item.tid) {
        item.tid = (Math.random() * 10).toString();
      }

      config = Object.assign({}, baseConfig, {
        Item: item
      });
      writeDb.putItem(config, (err, res) => {
        done(err, item);
      });
      break;
    case 'PUT':
      const todoItem = JSON.parse(event.body);
      config = Object.assign({}, baseConfig, {
        Key: {
          tlid: todoItem.tlid
        },
        UpdateExpression: 'set todoName = :n, todoComplete = :c',
        ExpressionAttributeValues: {
          ':n': todoItem.todoName,
          ':c': todoItem.todoComplete
        },
        ReturnValues: 'UPDATED_NEW'
      });

      writeDb.updateItem(config, done);
      break;
    case 'OPTIONS':
      done(null, '');
      break;
    default:
      done(new Error(`Unsupported method "${event.httpMethod}"`));
  }
};

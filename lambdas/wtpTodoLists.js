'use strict';

console.log('Loading function');

// Normal DynamoDB
const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();

const readDb = dynamo;
const writeDb = dynamo;

/**
 * Demonstrates a simple HTTP endpoint using API Gateway. You have full
 * access to the request and response payload, including headers and
 * status code.
 *
 * To scan a DynamoDB table, make a GET request with the TableName as a
 * query string parameter. To put, update, or delete an item, make a POST,
 * PUT, or DELETE request respectively, passing in the payload to the
 * DynamoDB API as a JSON body.
 */
exports.handler = (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  const done = (err, res) => callback(null, {
    statusCode: err ? '400' : '200',
    body: err ? err.message : JSON.stringify(res),
    headers: {
      'Content-Type': 'application/json',
      'Allow': 'OPTIONS, GET, HEAD, POST, PUT, DELETE',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'OPTIONS, GET, HEAD, POST, PUT, DELETE'
    }
  });

  console.log('Context:', JSON.stringify(context, null, 2));
  // TODO: get lodash working for _.get() here
  const baseConfig = {
    TableName: event.stageVariables.TABLE_NAME || 'wtp-todo-lists-dev'
  };
  let config = null;
  let tlid = null;

  switch (event.httpMethod) {
    case 'DELETE':
      // check for key on path:
      tlid = event.pathParameters && event.pathParameters.proxy;
      let TodoListItem = {};

      if(tlid && tlid.length) {
        TodoListItem.tlid = tlid;
      } else {
        TodoListItem = JSON.parse(event.body);
      }

      config = Object.assign({}, baseConfig, {
        Key: TodoListItem
      });

      writeDb.deleteItem(config, done);
      break;
    case 'GET':
      // if ID is provided, then lookup a single entry
      tlid = event.pathParameters && event.pathParameters.proxy;

      if(tlid && tlid.length) {
        config = Object.assign({}, baseConfig, {
          "FilterExpression": "tlid = :tlid OR listUrl = :tlid",
          "ExpressionAttributeValues": {
            ":tlid": tlid
          }
        });

        readDb.scan(config, (err, res) => {
          done(err, (res.Count > 0 ? res.Items[0] : null));
        });
      } else {
        // otherwise return them all
        readDb.scan(baseConfig, (err, res) => {
          done(err, res.Items);
        });
      }
      break;
    case 'POST':
      const item = JSON.parse(event.body);

      if(!item.tlid) {
        item.tlid = (Math.random() * 10).toString();
      }

      config = Object.assign({}, baseConfig, {
        Item: item
      });
      writeDb.putItem(config, (err, res) => {
        done(err, item);
      });
      break;
    case 'PUT':
      const listItem = JSON.parse(event.body);
      config = Object.assign({}, baseConfig, {
        Key: {
          tlid: listItem.tlid
        },
        UpdateExpression: 'set listName = :n, listUrl = :c',
        ExpressionAttributeValues: {
          ':n': listItem.listName,
          ':c': listItem.listUrl
        },
        ReturnValues: 'ALL_NEW'
      });

      writeDb.updateItem(config, done);
      break;
    case 'OPTIONS':
      done(null, '');
      break;
    default:
      done(new Error(`Unsupported method "${event.httpMethod}"`));
  }
};

module.exports = {
  accessKeyId: '',
  secretAccessKey: '',
  bucket: {
    dev: 'todo-dev.soatech.com',
    test: 'todo-test.soatech.com',
    prod: 'todo.soatech.com'
  }
};

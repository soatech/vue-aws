const _ = require('lodash');
const s3 = require('s3');
const optional = require('optional');
const s3config = optional('./publish/s3-config');
const env = process.argv[2] || 'production';

if (!s3config) {
    console.error('Please create an config/s3-config.js');
    process.exit();
}

const determineBucket = () => {
  if(_.includes(env, 'dev')) {
    return s3config.bucket.dev;
  } else if(_.includes(env, 'test')) {
    return s3config.bucket.test;
  } else if(_.includes(env, 'prod')) {
    return s3config.bucket.prod;
  }
};

const client = s3.createClient({
    maxAsyncS3: 20,     // this is the default
    s3RetryCount: 3,    // this is the default
    s3RetryDelay: 1000, // this is the default
    multipartUploadThreshold: 20971520, // this is the default (20 MB)
    multipartUploadSize: 15728640, // this is the default (15 MB)
    s3Options: {
        accessKeyId: s3config.accessKeyId,
        secretAccessKey: s3config.secretAccessKey
        // any other options are passed to new AWS.S3()
        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
    }
});

console.log(determineBucket());

const params = {
    localDir: 'dist',
    deleteRemoved: true, // default false, whether to remove s3 objects
                         // that have no corresponding local file.

    s3Params: {
        Bucket: determineBucket()
        // other options supported by putObject, except Body and ContentLength.
        // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
    }
};

const uploader = client.uploadDir(params);
uploader.on('error', (err) => {
    console.error('unable to sync:', err.stack);
});
uploader.on('progress', () => {
    console.log('progress', uploader.progressAmount, uploader.progressTotal);
});
uploader.on('end', () => {
    console.log('done uploading');
});

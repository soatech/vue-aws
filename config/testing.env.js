const merge = require('webpack-merge');
const devEnv = require('./development.env');

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"'
});
